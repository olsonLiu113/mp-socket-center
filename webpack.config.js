const path = require('path')
const webpack = require('webpack')
const CopyWebpackPlugin = require('copy-webpack-plugin');
const nodeExternals = require('webpack-node-externals')

module.exports = (env, options) => {
    console.log(`Application is running as ${env.NODE_ENV} mode.`);
    console.log(`Webpack is running as ${options.mode} mode.`);

    var config = {
        entry: {
            socketCenter: './src/index.js',
        },
        output: {
            path: path.join(__dirname, 'output'),
            publicPath: '/',
            filename: '[name].js'
        },
        target: 'node',
        node: {
            __dirname: false,
            __filename: true,
        },
        externals: [nodeExternals()],
        module: {
            rules: [
                {
                    test: /\.js$/,
                    exclude: [/node_modules/],
                    use: {
                        loader: "babel-loader"
                    }
                }
            ]
        },
        plugins: [
            new webpack.DefinePlugin({
                'process.env': { NODE_ENV: `'${env.NODE_ENV}'`}
            })
        ]
    }

    var copys = [
        {
            from: path.resolve(__dirname, './config.json'),
            to: path.resolve(__dirname, './output/config.json'),
            ignore: ['.*']
        },
        {
            from: path.resolve(__dirname, './cert'),
            to: path.resolve(__dirname, './output/cert'),
            ignore: ['.*']
        }
    ]

    if(env.NODE_ENV === 'production') {
        config.module.rules[0].exclude.push(/fakeClient/);
    } else {
        copys.push({
            from: path.resolve(__dirname, './fakeClient/fake-client.html'),
            to: path.resolve(__dirname, './output/fakeClient/fake-client.html'),
            ignore: ['.*']
        });
    }

    config.plugins.push(new CopyWebpackPlugin(copys));

    return config;
}