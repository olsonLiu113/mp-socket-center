import EVENT from '@/constant/event';
import constant from '@/constant/constant';
import jwtTool from 'jsonwebtoken';
import config from '@/config/config';
import path from 'path';
import log4jConfig from '@/config/log4j';
import infoHandler from '@/handlers/infoHandler';
import joinHandler from '@/handlers/joinHandler';
import disconnectHandler from '@/handlers/disconnectHandler';
import leaveHandler from '@/handlers/leaveHandler';
import gameLogHandler from '@/handlers/gameLogHandler';
import fireHandler from '@/handlers/fireHandler';
import hitHandler from '@/handlers/hitHandler';
import skillHandler from '@/handlers/skillHandler';
import skillHitHandler from '@/handlers/skillHitHandler';
import syncHandler from '@/handlers/syncHandler';
import equipHandler from '@/handlers/equipHandler';
import balancesHandler from '@/handlers/balancesHandler';
import messageHandler from '@/handlers/messageHandler';

const logger = log4jConfig(path.basename(__filename));

function checkRoomLevel(joinData, player) {
  if(joinData.roomLevel == null) {
    logger.error(`require determined room level`);
    return;
  }
  player.roomLevel = joinData.roomLevel;
}

function initPlayer(jwt) {
  let player = {};
  player.operatorId = jwt.operator;
  player.brandId = jwt.brandId;
  player.currency = jwt.currency;
  player.mode = jwt.mode;

  if (player.mode == constant.mode.fun){
    player.playerId = jwt.operator + config.idDelimiter + 'fun' +
      Math.floor(new Date().getTime()).toString().substring(5,11) +
      Math.floor(Math.random() * 100);
    player.balance = 100000;
  } else {
    player.playerId = [jwt.operator, jwt.player].filter(Boolean).join(config.idDelimiter);
    player.balance = jwt.balance;
  }

  return player;
}

export default nsp => {
  const gameId = nsp.name.substr(1);
  nsp.on('connection', socket => {
    const session = socket.handshake.query.session;
    const jwt = jwtTool.verify(session, config.jwtSecretKey);

    // register player own room
    const accountRoom = `player_${gameId}_${jwt.operator}_${jwt.player}`;
    socket.join(accountRoom);

    const player = initPlayer(jwt);

    socket.on(EVENT.INFO, () => {
      const roomId = Object.keys(socket.rooms).filter(item => item != socket.id)[0];
      infoHandler(socket, player.operatorId, player.playerId, player.currency, player.balance, roomId);
    });

    socket.on(EVENT.JOIN, data => {
      checkRoomLevel(data, player);
      joinHandler(nsp, socket, gameId, player.operatorId, player.playerId, player.balance, player.currency, player.mode, data);
    });

    socket.on(EVENT.GAME.FIRE, data => {
      const roomId = Object.keys(socket.rooms).filter(item => item != socket.id)[0];
      fireHandler(nsp, socket, roomId, player.playerId, data);
    });

    socket.on(EVENT.GAME.HIT, data => {
      const roomId = Object.keys(socket.rooms).filter(item => item != socket.id)[0];
      hitHandler(socket, gameId, roomId, player.roomLevel, player.operatorId, player.brandId, player.playerId, player.currency, player.mode, data);
    });

    socket.on(EVENT.GAME.SKILL, data => {
      const roomId = Object.keys(socket.rooms).filter(item => item != socket.id)[0];
      skillHandler(nsp, socket, gameId, roomId, player.playerId, data);
    });

    socket.on(EVENT.GAME.SKILLHIT, data => {
      const roomId = Object.keys(socket.rooms).filter(item => item != socket.id)[0];
      skillHitHandler(nsp, socket, gameId, roomId, player.roomLevel, player.operatorId, player.brandId, player.playerId, player.currency, player.mode, data);
    });

    socket.on(EVENT.GAME.EQUIP, data => {
      const roomId = Object.keys(socket.rooms).filter(item => item != socket.id)[0];
      equipHandler(nsp, gameId, roomId, data);
    });

    socket.on(EVENT.GAME.SYNC, data => {
      const roomId = Object.keys(socket.rooms).filter(item => item != socket.id)[0];
      syncHandler(nsp, gameId, roomId, socket.id);
    });

    socket.on(EVENT.BALANCES, data => {
      const roomId = Object.keys(socket.rooms).filter(item => item != socket.id)[0];
      balancesHandler(socket, roomId);
    });

    socket.on(EVENT.MESSAGE, data => {
      const roomId = Object.keys(socket.rooms).filter(item => item != socket.id)[0];
      messageHandler(nsp, roomId, player.playerId, data);
    });

    socket.on(EVENT.LEAVE, data => {
      leaveHandler(nsp, socket, player, gameId);
    });

    socket.on(EVENT.GAMELOG, data => {
      gameLogHandler(player.operatorId, gameId, player.brandId, player.playerId, player.mode, data);
    });

    socket.on('disconnect', reason => {
      disconnectHandler(nsp, socket, player.roomLevel, gameId, player.operatorId, player.brandId, player.roundId, player.playerId, player.mode);
    });

  });
}