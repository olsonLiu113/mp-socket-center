import http from 'http';
import https from 'https';
import express from 'express';
import socket from 'socket.io';
import fs from 'fs';
import websocketService from '@/socket';
import redisService from '@/services/redisService';
import checkSession from '@/checkSession';
import config from '@/config/config';
import path from 'path';
import log4jConfig from '@/config/log4j';
import mqConsumer from '@/messageQueue/mqConsumer';

const logger = log4jConfig(path.basename(__filename));

logger.info(`Application Environment: ${process.env.NODE_ENV}`);

if (process.env.NODE_ENV != 'production') {
  let fakeClient = require('@/../fakeClient/fake-client');
}

const app = express();
let server;

if (config.http.ssl === true) {
  server = https.createServer({
    key: fs.readFileSync(path.resolve(__dirname, config.http.keyPath)),
    cert: fs.readFileSync(path.resolve(__dirname, config.http.certPath))
  }, app);
} else {
  server = http.createServer(app);
}

const SOCKET_PORT = config.websocket.port;

redisService
  .flushall()
  .subscribe(result => {
    logger.info(`Redis initialization: ${result}`);

    const io = socket(server, config.websocket);
    server.listen(SOCKET_PORT);

    checkSession(io, 'tokyokombat');
    websocketService(io.of('/tokyokombat'));
    mqConsumer(io, 'tokyokombat');

    logger.info(`SocketCenter running on port ${SOCKET_PORT}`);
  }, err => {
    logger.error(err);
  });









