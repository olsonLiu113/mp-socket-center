import kafka from 'kafka-node';
import config from '@/config/config';
import path from 'path';
import log4jConfig from '@/config/log4j';
const logger = log4jConfig(path.basename(__filename));

const client = new kafka.KafkaClient({kafkaHost : `${config.kafka.host}:${config.kafka.port}`});
const producer = new kafka.HighLevelProducer(client);

producer.on('ready', () => {
  logger.info("Kafka server connected");
});

producer.on('error', err => {
  logger.error(err);
});

export default {
  sendMessage (data) {
    producer.send([{
      topic : config.kafka.topic.event,
      messages: JSON.stringify(data)
    }], (err, data) => {
      if(err) logger.error(`error on MQ Event sending: ${err} `);
    });
  },
  sendSettleEvent (data) {
    producer.send([{
      topic : config.kafka.topic.settle,
      messages: JSON.stringify(data)
    }], (err, data) => {
      if(err) logger.error(`error on MQ Settle sending: ${err} `);
    });
  },
  requestGameLog (data) {
    producer.send([{
      topic : config.kafka.topic.gamelogReq,
      messages: JSON.stringify(data)
    }], (err, data) => {
      if(err) logger.error(`error on MQ Settle sending: ${err} `);
    });
  }
}
