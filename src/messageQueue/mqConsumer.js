import NP from 'number-precision';
import kafka from 'kafka-node';
import config from '@/config/config';
import gameConfig from '@/config/gameConfig';
import EVENT from '@/constant/event';
import error from '@/constant/error';
import balanceDispatcher from '@/dispatchers/balanceDispatcher';
import roundDispatcher from '@/dispatchers/roundDispatcher';
import constant from '@/constant/constant';
import util from '@/utils/util';
import path from 'path';
import log4jConfig from '@/config/log4j';
const logger = log4jConfig(path.basename(__filename));

function composeAnnounce(awardFish, playerId, roomId) {
  return {
    playerId: playerId,
    roomId: roomId,
    fishId: awardFish.fishId,
    multiplier: awardFish.multiplier
  }
}

function composeResponse(playerId, mqData, balance) {
  let result = {
    playerId: playerId,
    fishes: mqData.fishes,
    totalWin: mqData.totalWin,
    balance: balance,
    ts: new Date().getTime()
  };
  if (mqData.event == 'SkillHit') {
    result.skillId = mqData.skillId;
  }
  return result;
}

function getAnnounceFishes(mqData) {
  if (mqData.fishes) {
    return mqData.fishes.filter(data =>
      (data.multiplier && Number(data.multiplier) >= gameConfig.announce.multiplier));
  }
}

export default (io, gameId) => {
  const rewardConsumer = new kafka.ConsumerGroup(
    {
      kafkaHost: `${config.kafka.host}:${config.kafka.port}`,
      groupId: config.kafka.group.reward
    },
    [config.kafka.topic.result]
  );

  rewardConsumer.on('message', mqObject => {
    if (mqObject == null || mqObject.value == null) return;
    const mqData = JSON.parse(mqObject.value);

    if(mqData.error) {
      const accountRoom = `player_${gameId}_${mqData.operatorId}_${mqData.playerId}`;
      logger.error(`${mqData.playerId} fail`);
      logger.error(`${mqData.error.code} : ${mqData.error.message}` );
      io.of(`/${gameId}`).to(accountRoom).emit(EVENT.ERROR, error.INTERNAL_SERVER_ERROR);
      io.of(`/${gameId}`).to(accountRoom).emit(EVENT.LOGOUT, error.REWARD_ERROR.code);
      return;
    }

    //generate unique player id
    let uniPlayerId = [mqData.operatorId, mqData.playerId].filter(Boolean).join(config.idDelimiter);

    let balance = balanceDispatcher.getPlayerBalance(uniPlayerId, mqData.roomId);

    if(balance != null) {
      let creditedBalance = util.getTwoDecimalFloat(balance);
      if (mqData.totalWin) {
        creditedBalance = NP.plus(creditedBalance, Number(mqData.totalWin));
        balanceDispatcher.setRoomBalance(uniPlayerId, mqData.roomId, creditedBalance);
      }

      // set to round
      roundDispatcher.allocate(uniPlayerId, mqData);

      io.of(`/${gameId}`).to(mqData.roomId).emit(EVENT.SERVER.REWARD, composeResponse(uniPlayerId, mqData, creditedBalance));

      // announce
      let announceFishes = getAnnounceFishes(mqData);
      if (mqData.mode == constant.mode.real && announceFishes.length > 0) {
        announceFishes.forEach(fish => {
          const announceMsg = composeAnnounce(fish, uniPlayerId, mqData.roomId);
          io.of(`/${gameId}`).emit(EVENT.ANNOUNCE, announceMsg);
        });
      }
    }
  }),

  rewardConsumer.on('error', mqObject => {
    logger.debug(`kafka consumer error : ${JSON.stringify(mqObject)}`)
  });

  const settleErrorConsumer = new kafka.ConsumerGroup(
    {
      kafkaHost: `${config.kafka.host}:${config.kafka.port}`,
      groupId: config.kafka.group.settle
    },
    [config.kafka.topic.settleError]
  );

  settleErrorConsumer.on('message', mqObject => {
    if (mqObject == null || mqObject.value == null) return;
    const mqData = JSON.parse(mqObject.value);

    if(mqData.error && mqData.operatorId && mqData.gameId && mqData.playerId) {
      const accountRoom = `player_${mqData.gameId}_${mqData.operatorId}_${mqData.playerId}`;
      logger.error(`player : ${mqData.playerId} access failed, message : ${JSON.stringify(mqData.error)}`);
      io.of(`/${mqData.gameId}`).to(accountRoom).emit(EVENT.LOGOUT, error.SETTLE_ERROR.code);
    }
  });

  settleErrorConsumer.on('error', mqObject => {
    logger.debug(`kafka consumer error : ${JSON.stringify(mqObject)}`)
  });

  const gameLogConsumer = new kafka.ConsumerGroup(
    {
      kafkaHost: `${config.kafka.host}:${config.kafka.port}`,
      groupId: config.kafka.group.gamelog
    },
    [config.kafka.topic.gamelogRes]
  );

  gameLogConsumer.on('message', mqObject => {
    if (mqObject == null || mqObject.value == null) return;
    const mqData = JSON.parse(mqObject.value);

    if(mqData.operatorId && mqData.gameId && mqData.playerId) {
      const accountRoom = `player_${mqData.gameId}_${mqData.operatorId}_${mqData.playerId}`;
      io.of(`/${mqData.gameId}`).to(accountRoom).emit(EVENT.GAMELOG, mqData);
    }
  });

  gameLogConsumer.on('error', mqObject => {
    logger.debug(`kafka consumer error : ${JSON.stringify(mqObject)}`)
  });
}