import { timer } from 'rxjs';
import loopExecutor from '@/room/loopExecutor';
import log4jConfig from '@/config/log4j';
import path from 'path';

const logger = log4jConfig(path.basename(__filename));

var timerGroup = {};

export default {
  setRoomTimer (nsp, gameId, roomId) {
    if (timerGroup[roomId]) return;

    timerGroup[roomId] = timer(2000, 1000).subscribe({
      next: loopExecutor(nsp, gameId, roomId),
      error: err => logger.error(err)
    });
  },

  closeRoomTimer(roomId) {
    if(timerGroup[roomId]) {
      timerGroup[roomId].unsubscribe();
      delete timerGroup[roomId];
    }
  }
}