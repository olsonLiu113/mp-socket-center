import config from '@/config/config';
import constant from '@/constant/constant';
import mqProducer from '@/messageQueue/mqProducer';
import { interval } from 'rxjs';
import { filter } from 'rxjs/operators';

import path from 'path';
import log4jConfig from '@/config/log4j';
const logger = log4jConfig(path.basename(__filename));

const roundSource = interval(config.roundIdUpdateMs);
var roundTimerGroup = {};
var roundBuffer = {};

function dumpRoundBuffer(playerId) {
  if(!roundBuffer[playerId]) return;

  const roundId = `${playerId}${new Date().getTime()}`;

  roundBuffer[playerId].map(buffer => {
    buffer.roundId = roundId;
    buffer.roundEndTs = new Date().getTime();
  });
  mqProducer.sendSettleEvent(roundBuffer[playerId]);
  delete roundBuffer[playerId];
}

function setRoundBuffer(playerId, mqData) {
  if (!roundBuffer[playerId]) {
    roundBuffer[playerId] = [];
  }
  mqData.roundStartTs = new Date().getTime();
  roundBuffer[playerId].push(mqData);
}

function setRoundTimer(playerId) {
  if (roundTimerGroup[playerId]) return;

  roundTimerGroup[playerId] = roundSource.pipe(
    filter(val => roundBuffer[playerId] != null)
  ).subscribe({
    next(val) {dumpRoundBuffer(playerId)},
    error: err => logger.error(err)
  });
}

export default {
  closeRound(playerId) {
    //close timer
    if (roundTimerGroup[playerId]) {
      roundTimerGroup[playerId].unsubscribe();
      delete roundTimerGroup[playerId];
    }
    //close buffer
    if (roundBuffer[playerId]) {
      dumpRoundBuffer(playerId);
      delete roundBuffer[playerId];
    }
  },

  allocate(uniPlayerId, mqData) {
    if (mqData.mode == constant.mode.fun) return;

    setRoundBuffer(uniPlayerId, mqData);
    setRoundTimer(uniPlayerId);
  }
}