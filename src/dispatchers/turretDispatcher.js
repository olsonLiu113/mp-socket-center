import redisService from '@/services/redisService';
import redisKey from '@/services/redisKeyService';
import { map } from 'rxjs/operators';
import path from 'path';
import log4jConfig from '@/config/log4j';

const logger = log4jConfig(path.basename(__filename));

export default {
  equipTurret(gameId, roomId, position, bulletId) {
    const turretKey = redisKey.turret(gameId, roomId);

    return redisService.hset(turretKey, position, bulletId).pipe(
      map(r => ({ position: position, bulletId: bulletId }))
    );
  },

  getRoomTurrets(gameId, roomId) {
    return redisService.hgetall(redisKey.turret(gameId, roomId)).pipe(
      map(data => {
        if(data == null) return {};
        for (let [key, value] of Object.entries(data)) {
          data[key] = parseInt(value);
        }
        return data;
      })
    );
  }

}