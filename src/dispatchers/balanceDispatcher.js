import log4jConfig from '@/config/log4j';
import path from 'path';

const logger = log4jConfig(path.basename(__filename));

let roomBalance = {};

function initial(playerId, roomId) {
  if(!roomBalance[roomId]) {
    roomBalance[roomId] = {};
  }
  if(!roomBalance[roomId][playerId]) {
    roomBalance[roomId][playerId] = {};
  }
}

export default {
  setRoomBalance(playerId, roomId, balance, currency) {
    initial(playerId, roomId);

    roomBalance[roomId][playerId].balance = balance;
    if(currency) roomBalance[roomId][playerId].currency = currency;
  },

  getRoomBalance(roomId) {
    return roomBalance[roomId];
  },

  getPlayerBalance(playerId, roomId) {
    if(roomBalance[roomId] && roomBalance[roomId][playerId]) {
      return roomBalance[roomId][playerId].balance;
    }
    return null;
  },

  removeRoomBalance(roomId) {
    delete roomBalance[roomId];
  },

  removePlayerBalance(roomId, playerId) {
    delete roomBalance[roomId][playerId];
  }
}