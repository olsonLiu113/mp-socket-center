import path from 'path';
import log4jConfig from '@/config/log4j';
import gameConfig from '@/config/gameConfig';
import redisService from '@/services/redisService';
import redisKey from '@/services/redisKeyService';
import { of } from 'rxjs';
import { map, mergeMap } from 'rxjs/operators';

const logger = log4jConfig(path.basename(__filename));

export default {
  record(gameId, roomId, data) {
    const historyKey = redisKey.history(gameId, roomId);

    of(data).pipe(
      mergeMap(current => redisService.lpush(historyKey, JSON.stringify(data))),
      mergeMap(r => redisService.llen(historyKey)),
      mergeMap(length => {
        if(length > gameConfig.history.size) {
          return redisService.rpop(historyKey);
        }
        return of(1);
      }))
      .subscribe(r => {
      });
  },

  getHistory(gameId, roomId) {
    const historyKey = redisKey.history(gameId, roomId);
    return of(historyKey).pipe(
      mergeMap(key => redisService.lrange(key, 0, -1)),
      map(data => {
        if(data == null) {
          return {};
        }
        return JSON.parse(`[${data}]`);
      })
    )
  }
/*
  cleanHistory(gameId, roomId) {
    const historyKey = redisKey.history(gameId, roomId);
    return redisService.del(historyKey);
  }*/
}