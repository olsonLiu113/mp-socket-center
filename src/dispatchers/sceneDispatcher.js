import path from 'path';
import gameConfig from '@/config/gameConfig';
import log4jConfig from '@/config/log4j';
import redisService from '@/services/redisService';
import redisKey from '@/services/redisKeyService';
import { zip, of } from 'rxjs';
import { map, mergeMap } from 'rxjs/operators';

const logger = log4jConfig(path.basename(__filename));

function getShuffledScene() {
  let scenes = gameConfig.scene.scenes.slice();
  for (let i = scenes.length - 1; i > 0; i--) {
    let randomIndex = Math.floor(Math.random() * i);
    [scenes[i], scenes[randomIndex]] = [scenes[randomIndex], scenes[i]];
  };
  return scenes;
};

function createScene() {
  const newPlayList = getShuffledScene();
  return {
    current: newPlayList.shift(),
    explosion: 0,
    summon: 0,
    timestop: 0,
    playList: newPlayList,
  };
};

export default {
  getCurrentScene(gameId, roomId) {
    const sceneKey = redisKey.scene(gameId, roomId);
    return redisService.hgetall(sceneKey).pipe(
      mergeMap(scene => {
        if (!scene) {
          scene = createScene();
          return redisService.hmset(sceneKey,
            'current', scene.current,
            'explosion', scene.explosion,
            'summon', scene.summon,
            'timestop', scene.timestop,
            'playList', JSON.stringify(scene.playList))
            .pipe(map(r => scene));
        }
        return of(scene);
      }));
  },

  switchScene(gameId, roomId) {
    const sceneKey = redisKey.scene(gameId, roomId);
    return redisService.hgetall(sceneKey).pipe(
      mergeMap(scene => {
        if (scene == null || Object.keys(scene) == 0) {
          logger.warn(`Scene: room ${roomId} is null or empty`);
          scene = createScene();
        } else {
          scene.playList = JSON.parse(scene.playList);
        }

        if (scene.playList.length === 0) {
          scene.playList = getShuffledScene();
        }

        let nextScene = 0;
        if (scene.playList[0] != scene.current) {
          nextScene = scene.playList.shift();
        } else {
          nextScene = scene.playList.pop();
        }

        return zip(
          redisService.hmset(sceneKey,
            'current', nextScene,
            'explosion', 0,
            'summon', 0,
            'timestop', 0,
            'playList', JSON.stringify(scene.playList)),
          redisService.hdel(sceneKey, 'summonLock')
        ).pipe(map(r => nextScene));
      }));
  },

  explodeScene(sceneKey) {
    return redisService.hincrby(sceneKey, 'explosion', 1);
  },

  increaseSummonTime(sceneKey) {
    return redisService.hincrby(sceneKey, 'summon', 1);
  },

  increaseTimeStop(sceneKey) {
    return redisService.hincrby(sceneKey, 'timestop', 1);
  },

  lockSummonFlag(gameId, roomId, content) {
    return redisService.hset(redisKey.scene(gameId, roomId), 'summonLock', content);
  },

  cleanScene(gameId, roomId) {
    return redisService.del(redisKey.scene(gameId, roomId));
  }
}