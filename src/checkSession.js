import EVENT from '@/constant/event';
import config from '@/config/config';
import constant from '@/constant/constant';
import jwtTool from 'jsonwebtoken';
import log4jConfig from '@/config/log4j';
import error from '@/constant/error';
import path from 'path';
import { promisify } from 'util';
import { mergeMap } from 'rxjs/operators';
import { of, from, throwError } from 'rxjs';

const logger = log4jConfig(path.basename(__filename));

function checkJwtDataCorrect(jwt, isRealMode) {
  if (isRealMode) {
    return jwt.operator && jwt.currency && jwt.mode && jwt.player && jwt.balance;
  }
  return jwt.operator && jwt.currency && jwt.mode;
}

function verifyJWT(sessionString, secretKey) {
  const getAsync = promisify(jwtTool.verify).bind(jwtTool);
  return from(getAsync(sessionString, secretKey))
}

export default (io, gameId) => {
  io.use((socket, next) => {
    const session = socket.handshake.query.session;

    verifyJWT(session, config.jwtSecretKey).pipe(
      mergeMap(jwt => {
        const isRealMode = jwt.mode == constant.mode.real;
        if (!checkJwtDataCorrect(jwt, isRealMode)) {
          return throwError('Token verification failed.');
        }
        if (isRealMode) {
          const accountRoom = `player_${gameId}_${jwt.operator}_${jwt.player}`;
          io.of(`/${gameId}`).to(accountRoom).emit(EVENT.LOGOUT, error.DUPLICATE_LOGIN.code);
        }
        return of(jwt);
      })
    ).subscribe(jwt => {
      return next();
    }, err => {
      let customError  = new Error();
      customError.data = error.INVALID_TOKEN;
      return next(customError);
    });
  });
}
