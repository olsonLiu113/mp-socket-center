import log4js from 'log4js';
import config from '@/config/config';

const levels = {
    'trace': log4js.levels.TRACE,
    'debug': log4js.levels.DEBUG,
    'info': log4js.levels.INFO,
    'warn': log4js.levels.WARN,
    'error': log4js.levels.ERROR,
    'fatal': log4js.levels.FATAL
};

log4js.configure({
  appenders: {
    app: {
      type: "dateFile",
      filename: "log/app.log",
      pattern: ".yyyy-MM-dd",
      compress: true,
      daysToKeep: 30,
      keepFileExt: true,
      maxLogSize: 10485760
    },
    /*app: {
      type: "file",
      filename: "log/app.log",
      maxLogSize: 10485760,
      numBackups: 3
    },*/
    console : {
      type: "console",
      layout: {
        type: "colored"
      }
    },
    errorFile: {
      type: "file",
      filename: "log/errors.log"
    },
    errors: {
      type: "logLevelFilter",
      level: "ERROR",
      appender: "errorFile"
    }
  },
  categories: {
    default: {
      appenders: [
        "console",
        "app",
        "errors"
      ],
      level: "DEBUG"
    }
    /*,http: {
      appenders: [
        "access"
      ],
      level: "DEBUG"
    }*/
  }
})

export default (name, level) => {
    var logger = log4js.getLogger(name);
    logger.level = levels[level] || levels['debug'];
    return logger;
};

/*
exports.use = function (app, level) {
  //加载中间件
    app.use(log4js.connectLogger(log4js.getLogger('logInfo'), {
        level: levels[level] || levels['debug'],
      //格式化http相关信息
        format: ':method :url :status'
    }));
};
function judgePath(pathStr) {
    if (!fs.existsSync(pathStr)) {
        fs.mkdirSync(pathStr);
        console.log('createPath: ' + pathStr);
    }
}
*/