export default {

  //totalLife: 360,

  orderly: {
    teamType: 0,
    period: 36,
    count: 4
  },

  orderless: {
    teamType: 1,
    period: 6,
    count: 36
  },

  boss: {
    teamType: 2,
    comeBefore: 60,
    count: 20
  },

  scene: {
    period: 360,
    scenes: [0,1,2,3], //4
    countdown: 5
  },

  history: {
    size: 10
  },

  announce: {
    multiplier : 100
  },

  skillLimit: {
    summon: 6,
    timestop: 6
  },

  summonLock: {
    second: {
      animation: 67,
      action: 62
    },
    hash: {
      animation: 'init',
      action: 'lock'
    }
  }
}