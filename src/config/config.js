import fs from 'fs';
import path from 'path';

var configJson = fs.readFileSync(path.resolve(__dirname, './config.json'));
var config = JSON.parse(configJson);

export default config;
