export default {
  mode: {
    fun: "fun",
    real: "real"
  },
  skill: {
    explosion: 10004,
    summon: 10006,
    timestop: 10005
  },
  team: {
    url: "https://tw-dev.ganapati.tech:7072/mp-path/tokyokombat/team.json"
  }
}