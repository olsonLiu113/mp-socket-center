export default {
  JOIN: "join",
  LEAVE: "leave",
  INFO: "info",
  LOGOUT: "logout",
  ANNOUNCE: "announce",
  BALANCES: "balances",
  GAMELOG: "gamelog",
  GAME: {
    SYNC: "sync",
    FIRE: "fire",
    HIT: "hit",
    SKILL: "skill",
    SKILLHIT: "skillhit",
    BUY: "buy",
    EQUIP: "equip",
    REWARD: "reward"
  },
  SERVER: {
    SYNC: "sync",
    FIRE: "fire",
    SKILL: "skill",
    APPEAR: "appear",
    REWARD: "reward",
    SCENE: "scene",
    EQUIP: "equip",
    COUNTDOWN: "countdown"
  },
  ERROR: "system_error"
}