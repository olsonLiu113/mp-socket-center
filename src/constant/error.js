export default {
  INTERNAL_SERVER_ERROR : {
    "code" : "500",
    "message" : "Internal server error"
  },
  INVALID_TOKEN : {
    "code" : "400",
    "message" : "Invalid player/token"
  },
  INSUFFICIENT_BALANCE : {
    "code" : "406",
    "message" : "Insufficient balance"
  },
  SUMMON_REACH_LIMIT : {
    "code" : "407",
    "message" : "summon using reach limit"
  },
  TIME_STOP_REACH_LIMIT : {
    "code" : "408",
    "message" : "time stop using reach limit"
  },
  CANT_SUMMON : {
    "code" : "409",
    "message" : "can't summon now"
  },
  SETTLE_ERROR : {
    "code" : "410",
    "message" : "error on settling"
  },
  DUPLICATE_LOGIN : {
    "code" : "411",
    "message" : "duplicate login"
  },
  REWARD_ERROR : {
    "code" : "412",
    "message" : "error on rewarding"
  }
}