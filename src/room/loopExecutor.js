import EVENT from '@/constant/event';
import config from '@/config/gameConfig';
import util from '@/utils/util';
import sceneDispatcher from '@/dispatchers/sceneDispatcher';
import path from 'path';
import log4jConfig from '@/config/log4j';

const logger = log4jConfig(path.basename(__filename));

const countdownTime = config.scene.period - config.scene.countdown;

const bossAppear = config.scene.period - config.boss.comeBefore;

const lockSummonAnimation = config.scene.period - config.summonLock.second.animation;

const lockSummonAction = config.scene.period - config.summonLock.second.action;

function emitAndSave(nsp, team, gameId, roomId) {
  let data = {};
  data.ts = Math.floor(Date.now());
  data.teamType = team.teamType;
  data.teamId = util.getRandomNumber(team.count);
  nsp.to(roomId).emit(EVENT.SERVER.APPEAR, data);
  // historyDispatcher.record(gameId, roomId, data);
}

function isOrderlyPassed(second) {
  return (second < config.orderly.period) || ((second % config.scene.period) >= config.orderly.period)
}

function emitOrderly(nsp, gameId, roomId, milliSecond) {
  setTimeout(() => {
    emitAndSave(nsp, config.orderly, gameId, roomId);
  }, milliSecond);
}

function lockSummonOrNot(cycleSecond, gameId, roomId) {
  if(cycleSecond > 0 && cycleSecond % lockSummonAnimation == 0) {
    sceneDispatcher.lockSummonFlag(gameId, roomId, config.summonLock.hash.animation);
    return;
  }

  if(cycleSecond > 0 && cycleSecond % lockSummonAction == 0) {
    sceneDispatcher.lockSummonFlag(gameId, roomId, config.summonLock.hash.action);
    return;
  }
}

export default (nsp, gameId, roomId) => {
  return val => {
    if (val == 0) return;
    const cycleSecond = val % config.scene.period;
    //scene switching countdown
    if (cycleSecond > 0 && cycleSecond % countdownTime == 0) {
      nsp.to(roomId).emit(EVENT.SERVER.COUNTDOWN, { countdown: config.scene.countdown });
      return;
    }

    if (cycleSecond > 0 && cycleSecond % bossAppear == 0) {
      emitAndSave(nsp, config.boss, gameId, roomId);
      return;
    }

    if (cycleSecond == 0) {
      sceneDispatcher.switchScene(gameId, roomId)
      .subscribe(data => {
        nsp.to(roomId).emit(EVENT.SERVER.SCENE, { sceneId: data });
        emitOrderly(nsp, gameId, roomId, 2500);
      });
      return;
    }

    lockSummonOrNot(cycleSecond, gameId, roomId);

    if (val % config.orderless.period == 0 && isOrderlyPassed(val)) {
      emitAndSave(nsp, config.orderless, gameId, roomId);
      return;
    }
  }
}


