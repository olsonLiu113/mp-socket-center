import config from '@/config/config';

export default {
  getCurrentTimestamp() {
    return new Date().getTime();
  },
  getRandomNumber(x) {
    return Math.floor(Math.random() * x) + 1;
  },

  getTwoDecimalFloat(number) {
    return Math.round(Number(number) * 100) / 100;
  },

  getRandomSummonBoss(x) {
    return Math.floor(Math.random() * ((x - 5) + 1)) + 5;
  },

  disassemblePlayerId(playerId) {
    let [, ...splitted] = playerId.split(config.idDelimiter)
    return splitted.join(config.idDelimiter);
  }
}
