import EVENT from '@/constant/event';
import balanceDispatcher from '@/dispatchers/balanceDispatcher';
import log4jConfig from '@/config/log4j';
import path from 'path';
const logger = log4jConfig(path.basename(__filename));

function composeResponse(balanceResult) {
  const res = {};

  const balances = {};
  for(const [k, v] of Object.entries(balanceResult)) {
    balances[k] = {};
    balances[k].balance = v.balance;
    balances[k].currency = v.currency;
  }
  res.balances = balances;
  return res;
}

export default (socket, roomId) => {
  if(isNaN(Number(roomId))) return;

  let balanceResult = balanceDispatcher.getRoomBalance(roomId);
  socket.emit(EVENT.BALANCES, composeResponse(balanceResult));
}