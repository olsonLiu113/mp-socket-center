import NP from 'number-precision';
import getJSON from 'get-json';
import EVENT from '@/constant/event';
import constant from '@/constant/constant';
import mqProducer from '@/messageQueue/mqProducer';
import balanceDispatcher from '@/dispatchers/balanceDispatcher';
import redisService from '@/services/redisService';
import redisKey from '@/services/redisKeyService';
import util from '@/utils/util';
import gameConfig from '@/config/gameConfig';
import sceneDispatcher from '@/dispatchers/sceneDispatcher';
import { from, of, throwError, zip } from 'rxjs';
import { map, mergeMap } from 'rxjs/operators';
import error from '@/constant/error';
import log4jConfig from '@/config/log4j';
import path from 'path';
const logger = log4jConfig(path.basename(__filename));

function changeFieldIfDrop(skillHitData) {
  if (skillHitData.skillId > 20000) {
    skillHitData.worth = skillHitData.bet;
    skillHitData.bet = 0;
  }
}

function overSummonLimit(skillId, scene) {
  return (skillId == constant.skill.summon && scene.summon >= gameConfig.skillLimit.summon)
}

function overTimestopLimit(skillId, scene) {
  return (skillId == constant.skill.timestop && scene.timestop >= gameConfig.skillLimit.timestop)
}

function notPermitSummonAction(skillId, scene) {
  return (skillId == constant.skill.summon && scene.summonLock == gameConfig.summonLock.hash.action);
}

function explode(skillId, sceneKey) {
  if (skillId == constant.skill.explosion) {
    return sceneDispatcher.explodeScene(sceneKey);
  }
  return of(0);
}

function timestop(skillId, sceneKey) {
  if (skillId == constant.skill.timestop) {
    return sceneDispatcher.increaseTimeStop(sceneKey);
  }
  return of(0);
}

function summon(nsp, roomId, mqSentData, sceneKey) {
  if (mqSentData.skillId == constant.skill.summon) {
    let data = {};
    data.ts = Math.floor(Date.now());
    data.teamType = gameConfig.boss.teamType;
    let teamId = util.getRandomSummonBoss(gameConfig.boss.count);
    data.teamId = teamId;
    mqSentData.teamId = teamId;
    nsp.to(roomId).emit(EVENT.SERVER.APPEAR, data);
    return sceneDispatcher.increaseSummonTime(sceneKey);
  }
  return of(0);
}

function getBossFromTeams(sceneMonsters, randomNumber) {
  if (sceneMonsters["2"] && sceneMonsters["2"][randomNumber] && sceneMonsters["2"][randomNumber][0]) {
    return sceneMonsters["2"][randomNumber][0].fishId;
  }
  return 0;
}

function extractMonsterIdWhenSummon(gameId, roomId, mqSentData) {
  return zip(from(getJSON(constant.team.url)),
    sceneDispatcher.getCurrentScene(gameId, roomId)
  ).pipe(
    map(data => {
      const [teams, scene] = data;
      return getBossFromTeams(teams[scene.current], mqSentData.teamId);
    })
  );
}

export default (nsp, socket, gameId, roomId, roomLevel, operatorId, brandId, playerId, currency, mode, skillHitData) => {

  if (!skillHitData.skillId || !skillHitData.bet) return;

  changeFieldIfDrop(skillHitData);
  let mqData = {
    event: "SkillHit",
    operatorId: operatorId,
    playerId: util.disassemblePlayerId(playerId),
    brandId: brandId,
    gameId: gameId,
    currency: currency,
    roomId: parseInt(roomId),
    roomLevel: roomLevel,
    bulletId: skillHitData.bulletId,
    skillId: skillHitData.skillId,
    bet: skillHitData.bet,
    worth: skillHitData.worth,
    fishes: skillHitData.fishes,
    mode: mode
  }
  const sceneKey = redisKey.scene(gameId, roomId);
  redisService.hgetall(sceneKey).pipe(
    mergeMap(scene => {
      if (notPermitSummonAction(skillHitData.skillId, scene)) {
        return throwError(error.CANT_SUMMON);
      }
      if (overSummonLimit(skillHitData.skillId, scene)) {
        return throwError(error.SUMMON_REACH_LIMIT);
      }
      if (overTimestopLimit(skillHitData.skillId, scene)) {
        return throwError(error.TIME_STOP_REACH_LIMIT);
      }
      return of(mqData);
    }),
    mergeMap(sendData => {
      let balance = balanceDispatcher.getPlayerBalance(playerId, roomId);
      let bet = Number(skillHitData.bet);
      const debitedBalance = NP.minus(util.getTwoDecimalFloat(balance), bet);
      if (bet < 0 || debitedBalance < 0) {
        return throwError(error.INSUFFICIENT_BALANCE);
      }
      sendData.balance = debitedBalance;
      balanceDispatcher.setRoomBalance(playerId, roomId, debitedBalance);
      return of(sendData);
    }),
    mergeMap(sendData => {
      return zip(
        explode(skillHitData.skillId, sceneKey),
        timestop(skillHitData.skillId, sceneKey),
        summon(nsp, roomId, sendData, sceneKey)
      ).pipe(map(r => sendData));
    }),
    mergeMap(sendData => {
      // check fishId if is using summon
      if (sendData.skillId == constant.skill.summon) {
        return extractMonsterIdWhenSummon(gameId, roomId, sendData).pipe(
          map(fishId => {
            sendData.summonRef = fishId;
            delete sendData.teamId;
            return sendData;
          }));
      }
      return of(sendData);
    })
  ).subscribe(sendData => {
    mqProducer.sendMessage(sendData);
  }, err => {
    socket.emit(EVENT.ERROR, err);
  });
}