import mqProducer from '@/messageQueue/mqProducer';
import constant from '@/constant/constant';
import util from '@/utils/util';
import log4jConfig from '@/config/log4j';
import path from 'path';

const logger = log4jConfig(path.basename(__filename));

export default (operatorId, gameId, brandId, playerId, mode, data) => {
  if (mode == constant.mode.fun || !data.page || !data.pageSize) return;

  let mqData = {
    operatorId : operatorId,
    gameId : gameId,
    brandId : brandId,
    playerId : util.disassemblePlayerId(playerId),
    page : data.page,
    pageSize : data.pageSize,
    roundId : data.roundId,
    skillId : data.skillId,
    timestamp : data.timestamp
  }
  mqProducer.requestGameLog(mqData);
}