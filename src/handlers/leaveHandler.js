import EVENT from '@/constant/event';
import constant from '@/constant/constant';
import redisService from '@/services/redisService';
import redisKey from '@/services/redisKeyService';
import log4jConfig from '@/config/log4j';
import balanceDispatcher from '@/dispatchers/balanceDispatcher';
import roundDispatcher from '@/dispatchers/roundDispatcher';
import path from 'path';
import { of, zip, throwError } from 'rxjs';
import { map, mergeMap, skipWhile } from 'rxjs/operators';
import roomDispatcher from '@/dispatchers/roomDispatcher';
import sceneDispatcher from '@/dispatchers/sceneDispatcher';

const logger = log4jConfig(path.basename(__filename));

function createRoomContext(roomId, roomLevel, roomPlayers, playerId) {
  let room = {};
  room.roomId = roomId;
  room.roomLevel = roomLevel;
  room.players = roomPlayers ? roomPlayers : {};
  room.playerId = playerId;
  return room;
}

function findPlayerPositionOfRoom(room, playerId) {
  if (!room || !playerId) {
    return;
  }
  return Object.keys(room.players).find(key => room.players[key] == playerId);
}

export default (nsp, socket, jwtPlayer, gameId) => {
  const playerInfoKey = redisKey.playerInfo(gameId, jwtPlayer.operatorId, jwtPlayer.playerId);

  redisService.hgetall(playerInfoKey).pipe(
    skipWhile(player => player == null || !player.roomId),
    mergeMap(player => {
      if (jwtPlayer.mode == constant.mode.real && (!player || Object.keys(player) == 0)) {
        return throwError(`${gameId}_${jwtPlayer.operatorId}_${jwtPlayer.playerId} not found.`);
      }
      const roomInfoKey = redisKey.roomInfo(gameId, player.roomId);

      return redisService.hgetall(roomInfoKey).pipe(
        map(roomPlayers => createRoomContext(player.roomId, jwtPlayer.roomLevel, roomPlayers, jwtPlayer.playerId))
      );
    }),
    skipWhile(room => {
      let position = findPlayerPositionOfRoom(room, jwtPlayer.playerId);
      return (position == 'undefined' || position == null);
    }),
    mergeMap(room => {
      const roomInfoKey = redisKey.roomInfo(gameId, room.roomId);
      const turretKey = redisKey.turret(gameId, room.roomId);
      const availableRoomKey = redisKey.availableRooms(gameId, room.roomLevel, jwtPlayer.mode);
      // set balance back to player initial object
      jwtPlayer.balance = balanceDispatcher.getPlayerBalance(jwtPlayer.playerId, room.roomId);

      let position = findPlayerPositionOfRoom(room, jwtPlayer.playerId);
      delete room.players[position];
      balanceDispatcher.removePlayerBalance(room.roomId, jwtPlayer.playerId);
      roundDispatcher.closeRound(jwtPlayer.playerId);
      return zip(
        redisService.hdel(turretKey, position),
        redisService.hdel(roomInfoKey, position),
        redisService.sadd(availableRoomKey, room.roomId),
        redisService.hdel(playerInfoKey, 'roomId')
      ).pipe(map(result => room));
    }),
    mergeMap(room => {
      if (Object.keys(room.players).length == 0) {
        roomDispatcher.closeRoomTimer(room.roomId);
        balanceDispatcher.removeRoomBalance(room.roomId);
        return sceneDispatcher.cleanScene(gameId, room.roomId).pipe(map(result => room));
      }
      return of(room);
    })
  )
    .subscribe(room => {
      socket.emit(EVENT.LEAVE, room);
      socket.leave(room.roomId);
      if (Object.keys(room.players).length > 0) {
        nsp.to(room.roomId).emit(EVENT.LEAVE, room);
      }
    }, err => {
      logger.error(err);
    });

}

