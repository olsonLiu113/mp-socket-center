import NP from 'number-precision';
import EVENT from '@/constant/event';
import mqProducer from '@/messageQueue/mqProducer';
import balanceDispatcher from '@/dispatchers/balanceDispatcher';
import error from '@/constant/error';
import util from '@/utils/util';
import log4jConfig from '@/config/log4j';
import path from 'path';
const logger = log4jConfig(path.basename(__filename));

export default (socket, gameId, roomId, roomLevel, operatorId, brandId, playerId, currency, mode, hitData) => {

  if (!hitData.bet) return;

  let mqData = {
    event: "Hit",
    operatorId: operatorId,
    playerId: util.disassemblePlayerId(playerId),
    brandId: brandId,
    gameId: gameId,
    roomId: parseInt(roomId),
    roomLevel: roomLevel,
    currency: currency,
    bet: hitData.bet,
    fishes: hitData.fishes,
    bulletId: hitData.bulletId,
    bulletX: hitData.bulletX,
    bulletY: hitData.bulletY,
    mode: mode
  };

  let balance = balanceDispatcher.getPlayerBalance(playerId, roomId);
  let bet = Number(hitData.bet);
  const debitedBalance = NP.minus(util.getTwoDecimalFloat(balance), bet);
  if (bet < 0 || debitedBalance < 0) {
    logger.debug(`player ${playerId} insufficient balance, original balance : ${balance}, debited balance : ${debitedBalance}`);
    socket.emit(EVENT.ERROR, error.INSUFFICIENT_BALANCE);
    return;
  }
  mqData.balance = debitedBalance;
  balanceDispatcher.setRoomBalance(playerId, roomId, debitedBalance);

  mqProducer.sendMessage(mqData);
}