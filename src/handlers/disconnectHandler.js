import EVENT from '@/constant/event';
import constant from '@/constant/constant';
import redisService from '@/services/redisService';
import redisKey from '@/services/redisKeyService';
import log4jConfig from '@/config/log4j';
import balanceDispatcher from '@/dispatchers/balanceDispatcher';
import roundDispatcher from '@/dispatchers/roundDispatcher';
import path from 'path';
import { of, zip, throwError } from 'rxjs';
import { map, mergeMap, skipWhile } from 'rxjs/operators';
import roomDispatcher from '@/dispatchers/roomDispatcher';
import sceneDispatcher from '@/dispatchers/sceneDispatcher';

const logger = log4jConfig(path.basename(__filename));

function createRoomContext(roomId, roomLevel, roomPlayers, playerId) {
  let room = {};
  room.roomId = roomId;
  room.roomLevel = roomLevel;
  room.players = roomPlayers ? roomPlayers : {};
  room.playerId = playerId;
  return room;
}

function findPlayerPositionOfRoom(room, playerId) {
  if (!room || !playerId) {
    return;
  }
  return Object.keys(room.players).find(key => room.players[key] == playerId);
}

export default (nsp, socket, roomLevel, gameId, operatorId, brandId, roundId, playerId, mode) => {
  const playerInfoKey = redisKey.playerInfo(gameId, operatorId, playerId);

  redisService.hgetall(playerInfoKey).pipe(
    skipWhile(player => player == null),
    mergeMap(player => {
      if (mode == constant.mode.real && (!player || Object.keys(player) == 0)) {
        return throwError(`${gameId}_${operatorId}_${playerId} not found.`);
      }

      if (player.roomId) {
        const roomInfoKey = redisKey.roomInfo(gameId, player.roomId);

        return redisService.hgetall(roomInfoKey).pipe(
          map(roomPlayers => createRoomContext(player.roomId, roomLevel, roomPlayers, playerId))
        );
      }

      return of({ playerId: playerId })
    }),
    mergeMap(room => {
      if (room.roomId) {
        const roomInfoKey = redisKey.roomInfo(gameId, room.roomId);
        const turretKey = redisKey.turret(gameId, room.roomId);
        const availableRoomKey = redisKey.availableRooms(gameId, room.roomLevel, mode);
        let position = findPlayerPositionOfRoom(room, playerId);
        delete room.players[position];
        balanceDispatcher.removePlayerBalance(room.roomId, playerId);
        roundDispatcher.closeRound(playerId);
        return zip(
          redisService.hdel(turretKey, position),
          redisService.hdel(roomInfoKey, position),
          redisService.sadd(availableRoomKey, room.roomId),
          redisService.del(playerInfoKey)
        ).pipe(map(result => room));
      }

      return redisService.del(playerInfoKey);
    }),
    mergeMap(room => {
      if (room.roomId && Object.keys(room.players).length == 0) {
        roomDispatcher.closeRoomTimer(room.roomId);
        balanceDispatcher.removeRoomBalance(room.roomId);
        return sceneDispatcher.cleanScene(gameId, room.roomId).pipe(map(result => room));
      }
      return of(room);
    })
  )
    .subscribe(room => {
      if (room.roomId && Object.keys(room.players).length > 0) {
        nsp.to(room.roomId).emit(EVENT.LEAVE, room);
      }
      socket.emit(EVENT.LOGOUT, playerId);
    }, err => {
      logger.error(err);
    });

}

