import EVENT from '@/constant/event';
import NP from 'number-precision';
import error from '@/constant/error';
import util from '@/utils/util';
import balanceDispatcher from '@/dispatchers/balanceDispatcher';
import path from 'path';
import log4jConfig from '@/config/log4j';

const logger = log4jConfig(path.basename(__filename));

export default (nsp, socket, roomId, playerId, fireData) => {
  if (!fireData.bet) return;
  let data = {};
  data.bulletId = fireData.bulletId;
  data.bulletX = fireData.bulletX;
  data.bulletY = fireData.bulletY;
  data.playerId = playerId;
  data.lock = fireData.lock;

  let balance = balanceDispatcher.getPlayerBalance(playerId, roomId);
  if (NP.minus(util.getTwoDecimalFloat(balance), Number(fireData.bet)) < 0) {
    socket.emit(EVENT.ERROR, error.INSUFFICIENT_BALANCE);
    return;
  }
  nsp.to(roomId).emit(EVENT.SERVER.FIRE, data);
}