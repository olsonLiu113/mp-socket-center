import EVENT from '@/constant/event';
import sceneDispatcher from '@/dispatchers/sceneDispatcher';
import turretDispatcher from '@/dispatchers/turretDispatcher';
import { zip } from 'rxjs';

import log4jConfig from '@/config/log4j';
import path from 'path';
const logger = log4jConfig(path.basename(__filename));

export default (nsp, gameId, roomId, socketId) => {
  zip(
    sceneDispatcher.getCurrentScene(gameId, roomId),
    turretDispatcher.getRoomTurrets(gameId, roomId)
  )
  .subscribe(data => {
    let syncData = {};
    syncData['ts'] = Math.floor(Date.now());
    syncData['sceneId'] = data[0].current;
    syncData['explosion'] = data[0].explosion;
    syncData['turrets'] = data[1];
    nsp.to(socketId).emit(EVENT.SERVER.SYNC, syncData);
  })
}


