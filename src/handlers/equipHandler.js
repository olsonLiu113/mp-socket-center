import turretDispatcher from '@/dispatchers/turretDispatcher';
import EVENT from '@/constant/event';
import log4jConfig from '@/config/log4j';
import path from 'path';
const logger = log4jConfig(path.basename(__filename));

export default (nsp, gameId, roomId, data) => {
  turretDispatcher.equipTurret(gameId, roomId, data.position, data.bulletId)
    .subscribe(equipped => {
      nsp.to(roomId).emit(EVENT.SERVER.EQUIP, equipped);
    });
}