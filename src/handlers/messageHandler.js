import log4jConfig from '@/config/log4j';
import EVENT from '@/constant/event';
import path from 'path';

const logger = log4jConfig(path.basename(__filename));

export default (nsp, roomId, playerId, data) => {
  let message = {
    playerId: playerId,
    message: data.message
  }

  nsp.to(roomId).emit(EVENT.MESSAGE, message);
}