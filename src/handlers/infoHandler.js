import EVENT from '@/constant/event';
import balanceDispatcher from '@/dispatchers/balanceDispatcher';
import path from 'path';
import log4jConfig from '@/config/log4j';
const logger = log4jConfig(path.basename(__filename));

export default (socket, operatorId, playerId, currency, initBalance, roomId) => {
  let data = {};
  data.operatorId = operatorId;
  data.currency = currency;
  data.playerId = playerId;

  if(isNaN(Number(roomId))) {
    data.balance = initBalance;
  } else {
    data.balance = balanceDispatcher.getPlayerBalance(playerId, roomId);
  }

  socket.emit(EVENT.INFO, data);
}