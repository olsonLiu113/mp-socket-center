import EVENT from '@/constant/event';
import redisService from '@/services/redisService';
import redisKey from '@/services/redisKeyService';
import config from '@/config/config';
import log4jConfig from '@/config/log4j';
import roomDispatcher from '@/dispatchers/roomDispatcher';
import balanceDispatcher from '@/dispatchers/balanceDispatcher';
import path from 'path';
import { throwError, of, zip } from 'rxjs';
import { tap, map, mergeMap } from 'rxjs/operators';

const logger = log4jConfig(path.basename(__filename));

function isRoomFull(players) {
  if (players.length < config.rooms.maxRoomSize) {
    return false;
  }
  for (var i = 0; i < config.rooms.maxRoomSize; i++) {
    if (!players[i]) {
      return false;
    }
  }
  return true;
}

function findPositionOfRoom(players, playerId) {
  for (let i = 0; i < config.rooms.maxRoomSize; i++) {
    if (players[i] == playerId || players[i] == null) {
      return i;
    }
  }
  return null;
}

function createRoomContext(roomId, players) {
  let room = {};
  room.roomId = roomId;
  room.players = players ? players : {};
  return room;
}

function composeResponse(room, playerId) {
  let roomAbout = {};
  roomAbout.roomId = room.roomId;
  roomAbout.playerId = playerId;
  roomAbout.players = room.players;
  return roomAbout;
}

export default function (nsp, socket, gameId, operatorId, playerId, balance, currency, mode, joinData) {
  if (!joinData.roomLevel && joinData.roomLevel != 0) return;
  const availableRoomsKey = redisKey.availableRooms(gameId, joinData.roomLevel, mode);

  redisService.spop(availableRoomsKey).pipe(
    mergeMap(roomId => { // get room id
      if (roomId == null) {
        return redisService.incr(redisKey.roomIdempotentKey(gameId))
      }
      return of(roomId);
    }),
    tap(roomId => roomDispatcher.setRoomTimer(nsp, gameId, roomId)), // set room to timer
    mergeMap(roomId => {  // get exist room or create new room
      const roomInfoKey = redisKey.roomInfo(gameId, roomId);
      return redisService.hgetall(roomInfoKey).pipe(map(players => createRoomContext(roomId, players)));
    }),
    mergeMap(room => {  // set player into room
      const position = findPositionOfRoom(room.players, room.playerId);
      if (position == null) {
        return throwError(`Unable to find a place in room ${room.roomId}`);
      }
      room.players[position] = playerId;
      const roomInfoKey = redisKey.roomInfo(gameId, room.roomId);
      const playerInfoKey = redisKey.playerInfo(gameId, operatorId, playerId);
      balanceDispatcher.setRoomBalance(playerId, room.roomId, balance, currency);
      return zip(
        redisService.hset(roomInfoKey, position, playerId),
        redisService.hset(playerInfoKey, 'roomId', room.roomId),
      ).pipe(map(result => room));
    }),
    mergeMap(room => {  // check room is still available or not
      if (!isRoomFull(room.players)) {
        return redisService.sadd(availableRoomsKey, room.roomId).pipe(map(result => room));
      }
      return of(room);
    })
  )
    .subscribe(room => {
      socket.join(room.roomId);
      let roomAbout = composeResponse(room, playerId);
      nsp.to(room.roomId).emit(EVENT.JOIN, roomAbout);
    }, err => {
      logger.error(err);
    });
}