import NP from 'number-precision';
import EVENT from '@/constant/event';
import error from '@/constant/error';
import util from '@/utils/util';
import gameConfig from '@/config/gameConfig';
import constant from '@/constant/constant';
import balanceDispatcher from '@/dispatchers/balanceDispatcher';
import { of, throwError } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import redisService from '@/services/redisService';
import redisKey from '@/services/redisKeyService';
import log4jConfig from '@/config/log4j';
import path from 'path';
const logger = log4jConfig(path.basename(__filename));

function overSummonLimit(skillId, scene) {
  return (skillId == constant.skill.summon && scene.summon >= gameConfig.skillLimit.summon)
}

function overTimestopLimit(skillId, scene) {
  return (skillId == constant.skill.timestop && scene.timestop >= gameConfig.skillLimit.timestop)
}

function notPermitSummonAnimation(skillId, scene) {
  return (skillId == constant.skill.summon && scene.summonLock);
}

export default (nsp, socket, gameId, roomId, playerId, skillData) => {
  if (!skillData.skillId || !skillData.bet) return;
  let broadcast = {};
  broadcast.skillId = skillData.skillId;
  broadcast.playerId = playerId;
  const sceneKey = redisKey.scene(gameId, roomId);
  redisService.hgetall(sceneKey).pipe(
    mergeMap(scene => {
      if (notPermitSummonAnimation(skillData.skillId, scene)) {
        return throwError(error.CANT_SUMMON);
      }
      if (overSummonLimit(skillData.skillId, scene)) {
        return throwError(error.SUMMON_REACH_LIMIT);
      }
      if (overTimestopLimit(skillData.skillId, scene)) {
        return throwError(error.TIME_STOP_REACH_LIMIT);
      }
      return of(broadcast);
    }),
    mergeMap(data => {
      let balance = balanceDispatcher.getPlayerBalance(playerId, roomId);
      const bet = Number(skillData.bet);
      if (bet < 0 || NP.minus(util.getTwoDecimalFloat(balance), bet) < 0) {
        return throwError(error.INSUFFICIENT_BALANCE);
      }
      return of(data);
    })
  ).subscribe(data => {
    nsp.to(roomId).emit(EVENT.SERVER.SKILL, data);
  }, err => {
    socket.emit(EVENT.ERROR, err);
  });
}