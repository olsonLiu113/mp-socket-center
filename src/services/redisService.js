import redis from 'redis';
import config from '@/config/config';
import { promisify } from 'util';
import { from } from 'rxjs';

import path from 'path';
import log4jConfig from '@/config/log4j';
const logger = log4jConfig(path.basename(__filename));

const client = redis.createClient(config.redis.port, config.redis.host);

client.on('connect', function () {
  logger.info('Redis client connected');
});

client.on('error', function (err) {
  logger.error('redis error :' + err);
});

export default {
  set(key, value) {
    const getAsync = promisify(client.set).bind(client);
    return from(getAsync(key, value));
  },

  get(key) {
    const getAsync = promisify(client.get).bind(client);
    return from(getAsync(key));
  },

  del(key) {
    const getAsync = promisify(client.del).bind(client);
    return from(getAsync(key));
  },

  hset(key, field, value) {
    const getAsync = promisify(client.hset).bind(client);
    return from(getAsync(key, field, value))
  },

  hget(key, field) {
    const getAsync = promisify(client.hget).bind(client);
    return from(getAsync(key, field));
  },

  hkeys(field) {
    const getAsync = promisify(client.hkeys).bind(client);
    return from(getAsync(field));
  },

  hlen(field) {
    const getAsync = promisify(client.hlen).bind(client);
    return from(getAsync(field));
  },

  hdel(key, field) {
    const getAsync = promisify(client.hdel).bind(client);
    return from(getAsync(key, field));
  },

  hmset(key, ...args) {
    const getAsync = promisify(client.hmset).bind(client);
    return from(getAsync(key, ...args));
  },

  hgetall(key) {
    const getAsync = promisify(client.hgetall).bind(client);
    return from(getAsync(key))
  },

  incr(key) {
    const getAsync = promisify(client.incr).bind(client);
    return from(getAsync(key));
  },

  hincrby(key, field, increment) {
    const getAsync = promisify(client.hincrby).bind(client);
    return from(getAsync(key, field, increment));
  },

  spop(key) {
    const getAsync = promisify(client.spop).bind(client);
    return from(getAsync(key));
  },

  srandmember(key) {
    const getAsync = promisify(client.srandmember).bind(client);
    return from(getAsync(key));
  },

  srem(key, content) {
    const getAsync = promisify(client.srem).bind(client);
    return from(getAsync(key, content));
  },

  sadd(key, content) {
    const getAsync = promisify(client.sadd).bind(client);
    return from(getAsync(key, content));
  },

  setex(key, seconds, value) {
    const getAsync = promisify(client.setex).bind(client);
    return from(getAsync(key, seconds, value));
  },

  lpush(key, content) {
    const getAsync = promisify(client.lpush).bind(client);
    return from(getAsync(key, content));
  },

  llen(key) {
    const getAsync = promisify(client.llen).bind(client);
    return from(getAsync(key));
  },

  lrange(key, start, stop) {
    const getAsync = promisify(client.lrange).bind(client);
    return from(getAsync(key, start, stop));
  },

  rpop(key) {
    const getAsync = promisify(client.rpop).bind(client);
    return from(getAsync(key));
  },

  flushall() {
    const getAsync = promisify(client.flushall).bind(client);
    return from(getAsync());
  }
}

