export default {

  roomIdempotentKey(gameId) {
    return `${gameId}_room_serial_id`;
  },

  session(gameId, operatorId, playerId) {
    return `session_${gameId}_${operatorId}_${playerId}`;
  },

  playerInfo(gameId, operatorId, playerId) {
    return `player_${gameId}_${operatorId}_${playerId}`;
  },

  roomInfo(gameId, roomId) {
    return `room_${gameId}_${roomId}`;
  },

  availableRooms(gameId, roomLevel, mode) {
    return `availableRooms_${gameId}_${roomLevel}_${mode}`;
  },

  scene(gameId, roomId) {
    return `scene_${gameId}_${roomId}`;
  },

  turret(gameId, roomId) {
    return `turret_${gameId}_${roomId}`;
  },

  history(gameId, roomId) {
    return `history_${gameId}_${roomId}`;
  }

};