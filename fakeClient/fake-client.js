import createError from 'http-errors'
import fs from 'fs'
import path from 'path'
import bodyParser from 'body-parser';
import express from 'express';
import ejs from 'ejs';
import redisService from '@/services/redisService'
import redisKey from '@/services/redisKeyService';
import config from '@/config/config';
import log4jConfig from '@/config/log4j';
import jwtTool from 'jsonwebtoken';
import { of } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

const PORT = 9098;

const logger = log4jConfig(path.basename(__filename));

const players = {
  andres : {
    "exp": 4102384997,
    "operator": "ganalogics",
    "player": "andres",
    "balance": "12115.00",
    "currency": "EUR",
    "brandId": "GANA",
    "mode": "real"
  },
  chris : {
    "exp": 4102384997,
    "operator": "ganalogics",
    "player": "chris",
    "balance": "8778.00",
    "currency": "CNY",
    "brandId": "GANA",
    "mode": "real"
  },
  ryan : {
    "exp": 4102384997,
    "operator": "ganalogics",
    "player": "ryan",
    "currency": "CNY",
    "balance": "9800.00",
    "brandId": "GANA",
    "mode": "real"
  },
  wilson : {
    "exp": 4102384997,
    "operator": "ganalogics",
    "player": "wilson",
    "balance": "8765.00",
    "currency": "CNY",
    "brandId": "HAPPYLUKE",
    "mode": "real"
  },
  frank : {
    "exp": 4102384997,
    "operator": "ganalogics",
    "player": "frank",
    "balance": "5678.00",
    "currency": "EUR",
    "brandId": "brand",
    "mode": "real"
  },
  charles : {
    "exp": 4102384997,
    "operator": "ganalogics",
    "player": "charles",
    "balance": "4466.00",
    "currency": "JPY",
    "brandId": "HAPPYLUKE",
    "mode": "real"
  },
  olson : {
    "exp": 4102384997,
    "operator": "ganalogics",
    "player": "olson",
    "currency": "USD",
    "balance": "4678.12",
    "brandId": "HAPPYLUKE",
    "mode": "real"
  },
  pov : {
    "exp": 4102384997,
    "operator": "ganalogics",
    "player": "pov",
    "balance": "100",
    "currency": "CNY",
    "mode": "real"
  },
  fun : {
    "exp": 4102384997,
    "operator": "ganalogics",
    "currency": "EUR",
    "balance": "null",
    "mode": "fun"
  },
  wrong: {
    "exp": 1573391031
  }
};

const app = express();
app.engine('html', ejs.renderFile);
app.set('view engine', 'html');
app.set('views', __dirname);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.get('/', (req, res) => {
  res.send(`Please specify a playerId.`);
})

app.get('/:id', (req, res) => {
  let requestPlayer = req.params.id;
  let player = players[requestPlayer];

  if (!player || !requestPlayer) {
    return res.send(`Please specify a correct player after the URL slash`);
  }

  let jwt = jwtTool.sign(player, config.jwtSecretKey);

  res.render(path.resolve(__dirname, './fakeClient/fake-client.html'), {
    url: config.http.domain,
    socketPort: config.websocket.port,
    jwt: jwt
  });
})

// catch 404 and forward to error handler
app.use((req, res, next) => {
  next(createError(404))
})

// error handler
app.use((err, req, res, next) => {
  logger.error(err);
  res.status(err.status || 500)
  res.send('')
})

app.listen(PORT, () => {
  logger.info(`FakeClient running on port ${PORT}`)
})
